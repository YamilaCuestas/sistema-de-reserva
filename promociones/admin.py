from django.contrib import admin
from . import models
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter

@admin.register(models.Promocion)
class PromocionAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'tipo_bonificacion', 'valor_bonificacion', 'desde','hasta','vigente')
	ordering = ['desde']
	search_fields = ('nombre',)
	