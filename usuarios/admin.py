from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin

from .models import Persona, User, LimiteReserva

class PersonaAdmin(admin.ModelAdmin):
	# modifica el orden del agregar y modificar
	#fields = ('dni','nombre', 'apellido','direccion')
	# lo que quiero que me muestre en el admin
	list_display = ('apellido','nombre', 'nro_dni','sexo','direccion','telefono')
	search_fields = ('apellido','nombre')
	ordering = ['apellido']

admin.site.register(Persona, PersonaAdmin)


class UsuarioAdmin(UserAdmin):
	model = User
	list_display = ('username','email', 'persona',)
	search_fields = ('username','persona__apellido','persona__nombre','persona__nro_dni')
	fieldsets = UserAdmin.fieldsets + ((None, {'fields': ('persona','limite_reserva')}),)
	

# no me juzguen
for (name, field_options) in UsuarioAdmin.fieldsets:
	if ('first_name' in field_options['fields']):
		field_options['fields'] = tuple(f for f in field_options['fields'] if f != 'first_name')
	if ('last_name' in field_options['fields']):
		field_options['fields'] = tuple(f for f in field_options['fields'] if f != 'last_name')
admin.site.register(User, UsuarioAdmin)

class LimiteReservaAdmin(admin.ModelAdmin):
	pass
admin.site.register(LimiteReserva, LimiteReservaAdmin)
