
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import login
from django.contrib.auth.views import login as LoginView
from django.views import View
from django.urls import resolve, reverse

from sistema_de_reserva import settings
from .forms import PersonaForm, RegistroForm
from .models import Persona, User, LimiteReserva
from reservas.models import Entrada,EstadoEntrada
from funciones.models import Funcion
# Create your views here.

class RegistrationView(View):
	def get(self, request, *args, **kwargs):
		persona_form = kwargs.get('persona_form', PersonaForm())
		registro_form = kwargs.get('registro_form', RegistroForm())
		return render(request, 'registration/registration_form.html', {
			'registro_form': registro_form,
			'persona_form': persona_form,
		})
	def post(self, request, *args, **kwargs):
		try:
			persona_instance = Persona.objects.get(nro_dni=request.POST.get('nro_dni', ''))
		except:
			persona_instance = None
		persona_form = PersonaForm(request.POST, instance=persona_instance)
		registro_form = RegistroForm(request.POST)
		if not persona_form.is_valid() or not registro_form.is_valid():
			return self.get(request, persona_form=persona_form, registro_form=registro_form)
		p = persona_form.save()
		u = registro_form.save(commit=False)
		u.persona = p
		limit,created = LimiteReserva.objects.get_or_create(nombre='Regular')
		u.limite_reserva = limit
		u.save()
		login(request, u)
		return redirect(reverse('inicio'))

class ReservationsView(View):
	def breadcrumbs():
		return [{
			'name': 'Mis Entradas',
			'url': reverse('accounts-reservations')
		}]
	def get(self, request, *args, **kwargs):
		reservations = request.user.estadoentrada_set.filter(estado=EstadoEntrada.R).all()
		reservations = tuple(r for r in reservations if r.is_newest())
		pending = {r.codigo for r in reservations}
		pending = [
			{'code': codigo, 'count': sum(1 for f in filter(lambda r: r.codigo==codigo, reservations))}
			for codigo in pending
		]

		paid = request.user.estadoentrada_set.filter(estado=EstadoEntrada.P).all()
		paid = (p for p in paid if p.is_newest())
		paid_codes = {}
		for p in paid:
			paid_codes[p.codigo] = paid_codes.get(p.codigo, []) + [p]
		paid_codes = (
			{'code': code, 'count': len(entries), 'funcion': entries[0].entrada.funcion, 'pago': entries[0].pago}
			for code,entries in paid_codes.items()
		)


		pendiente = request.user.estadoentrada_set.filter(estado=EstadoEntrada.R).all()
		pendiente = (p for p in pendiente if p.is_newest())
		pendiente_codes = {}
		for p in pendiente:
			pendiente_codes[p.codigo] = pendiente_codes.get(p.codigo, []) + [p]
		pendiente_codes = (
			{'code': code, 'count': len(entries), 'funcion': entries[0].entrada.funcion, 'pago': entries[0].pago}
			for code,entries in pendiente_codes.items()
		)

		return render(request, 'usuarios/view_reservations.html', {
			'reservations': reservations,
			'pending': pending,
			'paid_codes': paid_codes,
			'pendientes': pendiente_codes,
		})

def reservations_cancel_view(request):
	if request.method == 'POST':
		ee = get_object_or_404(EstadoEntrada, pk=request.POST['estadoentrada_id'])
		if ee.entrada.cancelable(request.user):
			ee.entrada.cancelar()
	return redirect(reverse('accounts-reservations'))
