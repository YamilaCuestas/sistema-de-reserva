

let int2letters = function(x, charset) {
	charset = charset || 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	let s = [x % charset.length];
	x = Math.floor(x/charset.length);
	while (x > 0) {
		s.splice(0, 0, Math.max(0, x % (charset.length+1) - 1))
		x = Math.floor(x / (charset.length+1))
	}
	s = s.map(function(idx) { return charset[idx]; });
	return s.join('');
}

function EntryMap(entradas, sala_columnas, sala_filas, table) {
	this.entradas = [];
	this.inputs = [];

	let thead = document.createElement('thead');
	thead.appendChild(document.createElement('td'));
	for (let i=0; i < sala_columnas; i++) {
		let td = document.createElement('td');
		td.appendChild(document.createTextNode(i+1));
		thead.appendChild(td);
	}
	table.appendChild(thead);

	let row = 0;
	let new_row = function() {
		let tr = document.createElement('tr');
		let td = document.createElement('td');
		td.appendChild(document.createTextNode(int2letters(row)));
		tr.appendChild(td);
		++row;
		return tr
	}
	let tr = new_row();

	for (let e of entradas) {
		let td = document.createElement('td');
		td.classList.add('entrada');
		if (e.butaca_disponibilidad != 'V') {
			td.appendChild(document.createTextNode(e.estado));

			let input = document.createElement('input');
			input.type = 'checkbox';
			input.name = 'entradas';
			input.value = e.id_entrada;

			td.appendChild(input);
			this.inputs.push(input);
			this.entradas.push(e);
		}

		tr.appendChild(td);

		if (e.butaca_columna == sala_columnas) {
			table.appendChild(tr);
			tr = new_row();
		}
	};
}

function SeatMap(butacas, sala_columnas, sala_filas, table) {
	this.butacas = butacas;
	this.inputs = [];

	let thead = document.createElement('thead');
	thead.appendChild(document.createElement('td'));
	for (let i=0; i < sala_columnas; i++) {
		let td = document.createElement('td');
		td.appendChild(document.createTextNode(i+1));
		thead.appendChild(td);
	}
	table.appendChild(thead);

	let row = 0;
	let new_row = function() {
		let tr = document.createElement('tr');
		let td = document.createElement('td');
		td.appendChild(document.createTextNode(int2letters(row)));
		tr.appendChild(td);
		++row;
		return tr
	}
	let tr = new_row();

	for (let b of butacas) {
		let td = document.createElement('td');
		td.classList.add('entrada');
		if (b.disponibilidad == 'V')
			td.classList.add('vacio');
		else if (b.disponibilidad == 'N')
			td.classList.add('normal');
		else if (b.disponibilidad == 'NR')
			td.classList.add('no-reservable');
		else if (b.disponibilidad == 'NC')
			td.classList.add('no-comprable')

		td.appendChild(document.createTextNode('\xa0'));
		let input = document.createElement('input');
		input.type = 'checkbox';
		input.name = 'butacas';
		input.value = b.id;
		td.appendChild(input);
		this.inputs.push(input);

		tr.appendChild(td);

		if (b.columna == sala_columnas) {
			table.appendChild(tr);
			tr = new_row();
		}
	};
}
