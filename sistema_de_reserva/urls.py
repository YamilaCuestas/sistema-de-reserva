"""sistema_de_reserva URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings
from . import views
from simulador.admin import bank_site

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^banco/', include(bank_site.urls)),
    url(r'accounts/', include('usuarios.urls')),
    url(r'peliculas/', include('peliculas.urls')),
    url(r'^$', views.CarteleraView.as_view(), name='inicio'),
    url(r'funciones/', include('funciones.urls')),
    url(r'reservas/', include('reservas.urls')),
	url(r'^reportes/', include('reportes.urls'))
]

urlpatterns += i18n_patterns(
    url(r'^admin/', admin.site.urls, name='admin'),
    url(r'accounts/', include('usuarios.urls')),
    prefix_default_language=False
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
