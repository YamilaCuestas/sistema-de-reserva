"""
WSGI config for sistema_de_reserva project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os
import sys

sys.path.append('/var/www/sistema-de-reserva')
sys.path.append('/usr/local/lib/python3.4/dist-packages')

from django.core.wsgi import get_wsgi_application


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sistema_de_reserva.settings")

application = get_wsgi_application()
