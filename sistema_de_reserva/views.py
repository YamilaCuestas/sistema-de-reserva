from django.views import View
from django.shortcuts import render
from django.urls import reverse

from peliculas.models import Pelicula, Genero, TipoVideo
from funciones.models import Precio, TipoPrecio 

class CarteleraView(View):
	def breadcrumbs():
		return [{
			'name': 'Cartelera',
			'url': reverse('inicio')
		}]
	def get(self, request):
		return render(request, "index.html", {
			'peliculas': Pelicula.objects.all(),
			'generos': Genero.objects.all(),
			'tipovideos': TipoVideo.objects.all(),
			'precios': Precio.objects.all(),
			'tipoprecios': TipoPrecio.objects.all()
		})
