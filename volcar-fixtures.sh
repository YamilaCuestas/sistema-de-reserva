#!/bin/bash

for app in peliculas salas funciones reservas simulador
do
	python3 manage.py dumpdata $app > $app/fixtures/$app.json
done
