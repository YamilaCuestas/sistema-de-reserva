
from django.db import models
from django.core.exceptions import ValidationError

from reservas.models import EstadoEntrada, Pago

class BaseCubeReport(models.Model):
	ROUTES = None
	def route(self, from_, to):
		l = [to]
		while from_ != to:
			src = next(src for src,dst in self.ROUTES.keys() if dst == to)
			l = [src] + l
			to = src
		return l
	def browse(self, from_, to, instance):
		r = self.route(from_, to)
		for src,dst in zip(r, r[1:]):
			if instance is None:
				return instance
			instance = self.ROUTES[(src,dst)](instance)
		return instance

	def get_facts(self):
		return None
	def get_fact_value(self, instance):
		return None
	def get_fact_name(self, plural=False):
		return None

	nombre = models.CharField(unique=True, max_length=50)

	agrupar = models.CharField('Agrupar por', max_length=4, blank=False, null=False)
	rollup = models.CharField('Hacer roll-up hasta', max_length=4, blank=True, null=True)

	SUM, AVG, MAX, MIN, CNT = 'SUM', 'AVG', 'MAX', 'MIN', 'CNT'
	AGREGACION_CHOICES = (
		(SUM, 'Suma'),
		(AVG, 'Promedio'),
		(MAX, 'Máximo'),
		(MIN, 'Mínimo'),
		(CNT, 'Cuenta')
	)
	agregacion = models.CharField('Agregar con', max_length=3, choices=AGREGACION_CHOICES, blank=True, null=True)

	def get_agrupar_display(self):
		return self.DIMENSION_NAMES[self.agrupar]
	def get_rollup_display(self):
		return self.DIMENSION_NAMES.get(self.rollup, '-')

	def clean(self):
		super().clean()
		super().clean_fields()
		if bool(self.rollup) != bool(self.agregacion):
			raise ValidationError('Si se especifica un campo de rollup se debe especificar una función de agregación y viceversa')

		if self.rollup == self.agrupar:
			raise ValidationError('No se puede hacer rollup de una dimensión hacia si misma')

		if self.rollup:
			try:
				self.route(self.agrupar, self.rollup)
			except:
				r = self.route(None, self.rollup)
				raise ValidationError('No se puede hacer roll-up desde %s hasta %s (debe agrupar por: %s)' % (
					self.get_agrupar_display(),
					self.get_rollup_display(),
					' o '.join(self.DIMENSION_NAMES[k] for k in r[1:-1])
				))


FU, SA, PE, TV, GE = 'FU', 'SA', 'PE', 'TV', 'GE'
PA, TA, BA, TT = 'PA', 'TA', 'BA', 'TT'
DS, ME = 'DS', 'ME'
DSF, MEF = 'DSF', 'MEF'

class EntradasVendidas(BaseCubeReport):
	DIMENSION_NAMES = {
		'DS': 'Día de la semana (venta)',
		'ME': 'Mes (venta)',
		'FU': 'Función',
		'PA': 'Pago',
		'DSF': 'Día de la semana (función)',
		'MEF': 'Mes (función)',
		'SA': 'Sala',
		'PE': 'Película',
		'TV': 'Tipo de video',
		'GE': 'Género',
		'TA': 'Tarjeta',
		'BA': 'Banco',
		'TT': 'Tipo de tarjeta'
	}
	ROUTES = {
		(None, FU): lambda ee: ee.entrada.funcion,
		(FU, SA): lambda f: f.sala,
		(FU, PE): lambda f: f.pelicula,
		(PE, TV): lambda p: p.tipo_video,
		(PE, GE): lambda p: p.genero,
		(FU, DSF): lambda f: f.fecha.strftime('%A'),
		(FU, MEF): lambda f: f.fecha.strftime('%B'),

		(None, PA): lambda ee: ee.pago,
		(PA, TA): lambda p: p.tarjeta,
		(TA, BA): lambda t: t.banco,
		(TA, TT): lambda t: t.tipo_tarjeta,

		(None, DS): lambda ee: ee.fecha.strftime('%A'),
		(None, ME): lambda ee: ee.fecha.strftime('%B')
	}

	def get_facts(self):
		return EstadoEntrada.objects.filter(
			fecha__gte=self.fecha_desde,
			fecha__lte=self.fecha_hasta,
			estado=EstadoEntrada.P
		)
	def get_fact_value(self, instance):
		return int(1)
	def get_fact_name(self, plural=False):
		return 'Entradas' if plural else 'Entrada'

	fecha_desde = models.DateTimeField()
	fecha_hasta = models.DateTimeField()

	gen_tabla = models.BooleanField('Generar tabla', default=True)
	gen_barras_v = models.BooleanField('Generar gráfico de barras vertical', default=True)
	gen_barras_h = models.BooleanField('Generar gráfico de barras horizontal', default=True)
	gen_torta = models.BooleanField('Generar gráfico de torta', default=True)

	texto_inicial = models.CharField(max_length=600, blank=True, null=True)
	texto_final = models.CharField(max_length=600, blank=True, null=True)

	class Meta:
		verbose_name = 'Reporte de entradas vendidas'
		verbose_name_plural = 'Reportes de entradas vendidas'

	def __str__(self):
		return self.nombre

	def clean(self):
		super().clean()
		self.clean_fields()
		if self.fecha_desde >= self.fecha_hasta:
			raise ValidationError('Las fechas determinan un intervalo vacío')

class Recaudacion(BaseCubeReport):
	DIMENSION_NAMES = {
		'DS': 'Día de la semana (venta)',
		'ME': 'Mes (venta)',
		'FU': 'Función',
		'DSF': 'Día de la semana (función)',
		'MEF': 'Mes (función)',
		'SA': 'Sala',
		'PE': 'Película',
		'TV': 'Tipo de video',
		'GE': 'Género',
		'TA': 'Tarjeta',
		'BA': 'Banco',
		'TT': 'Tipo de tarjeta'
	}
	ROUTES = {
		(None, FU): lambda p: p.estadoentrada_set.first().entrada.funcion,
		(FU, SA): lambda f: f.sala,
		(FU, PE): lambda f: f.pelicula,
		(PE, TV): lambda p: p.tipo_video,
		(PE, GE): lambda p: p.genero,
		(FU, DSF): lambda f: f.fecha.strftime('%A'),
		(FU, MEF): lambda f: f.fecha.strftime('%B'),

		(None, TA): lambda p: p.tarjeta,
		(TA, BA): lambda t: t.banco,
		(TA, TT): lambda t: t.tipo_tarjeta,

		(None, DS): lambda p: p.estadoentrada_set()[0].fecha.strftime('%A'),
		(None, ME): lambda p: p.estadoentrada_set()[0].fecha.strftime('%B')
	}
	class Meta:
		verbose_name = 'Reporte de recaudación'
		verbose_name_plural = 'Reportes de recaudacion'

	fecha_desde = models.DateTimeField()
	fecha_hasta = models.DateTimeField()

	gen_tabla = models.BooleanField('Generar tabla', default=True)
	gen_barras_v = models.BooleanField('Generar gráfico de barras vertical', default=True)
	gen_barras_h = models.BooleanField('Generar gráfico de barras horizontal', default=True)
	gen_torta = models.BooleanField('Generar gráfico de torta', default=True)

	texto_inicial = models.CharField(max_length=600, blank=True, null=True)
	texto_final = models.CharField(max_length=600, blank=True, null=True)

	def get_facts(self):
		return Pago.objects.filter(
			estadoentrada__fecha__gte=self.fecha_desde,
			estadoentrada__fecha__lte=self.fecha_hasta,
		).distinct()
	def get_fact_value(self, instance):
		return float(instance.monto)
	def get_fact_name(self, plural=False):
		return 'Recaudaciones' if plural else 'Recaudación'

	def __str__(self):
		return self.nombre

	def clean(self):
		super().clean()
		self.clean_fields()
		if self.fecha_desde >= self.fecha_hasta:
			raise ValidationError('Las fechas determinan un intervalo vacío')
