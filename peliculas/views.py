
from django.core.serializers import serialize
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.shortcuts import render
from django.views import View
from django.urls import reverse

from .models import Pelicula
from sistema_de_reserva.views import CarteleraView

# Create your views here.

class VerPeliculaView(View):
	def breadcrumbs(id_pelicula):
		return CarteleraView.breadcrumbs() + [{
			'name': Pelicula.objects.get(pk=id_pelicula).titulo,
			'url': reverse('ver-pelicula', args=[id_pelicula])
		}]
	def get(self, request, id_pelicula):
		pelicula = Pelicula.objects.get(pk=id_pelicula)
		return render(request, 'peliculas/ver-pelicula.html', {
			'pelicula': pelicula,
			'breadcrumbs': VerPeliculaView.breadcrumbs(pelicula.id)
		})

def raw_pelicula(request, id_pelicula, *args, **kwargs):
	p = Pelicula.objects.get(pk=id_pelicula)
	return HttpResponse(serialize('json', {p}), content_type='application/json')
