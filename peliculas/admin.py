from django.contrib import admin
from . import models
from django_admin_listfilter_dropdown.filters import DropdownFilter, RelatedDropdownFilter

# Filtros
class AudioFilter(admin.RelatedFieldListFilter):
    def has_output(self):
        return True

class GeneroFilter(admin.RelatedFieldListFilter):
    def has_output(self):
        return True

class SubtituloFilter(admin.RelatedFieldListFilter):
    def has_output(self):
        return True

class TipoVideoFilter(admin.RelatedFieldListFilter):
    def has_output(self):
        return True

@admin.register(models.Pelicula)
class PeliculaAdmin(admin.ModelAdmin):
    list_display = ('titulo','titulo_original','isan','audio','subtitulo','tipo_video','clasificacion',)
    add_form_template = 'admin/peliculas/change_pelicula.html'
    change_form_template = 'admin/peliculas/change_pelicula.html'
    ordering = ['titulo']
    # Busquedas
    search_fields = ('titulo',)
    list_filter = (
        ('tipo_video',RelatedDropdownFilter),
        ('audio',RelatedDropdownFilter),
        ('subtitulo',RelatedDropdownFilter),
        ('genero',RelatedDropdownFilter),
        ('clasificacion', DropdownFilter))
    ordering = ['titulo']


@admin.register(models.Genero)
class GeneroAdmin(admin.ModelAdmin):
    list_display = ('nombre','descripcion',)
    search_fields = ('nombre',)
    ordering = ['nombre']

@admin.register(models.TipoVideo)
class TipoVideoAdmin(admin.ModelAdmin):
    list_display = ('nombre','descripcion',)
    search_fields = ('nombre','descripcion',)
    ordering = ['nombre']


@admin.register(models.Idioma)
class IdiomaAdmin(admin.ModelAdmin):
    list_display = ('nombre',)
    search_fields = ('nombre',)
    ordering = ['nombre']
