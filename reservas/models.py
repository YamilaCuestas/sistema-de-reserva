from django.db import models
from django.db.models import Max

from salas.models import Butaca
from peliculas.models import Pelicula
from funciones.models import Funcion,Precio
from usuarios.models import User
from promociones.models import Promocion

class Banco(models.Model):
	nombre = models.CharField(unique=True, max_length=30)

	def __str__(self):
		return self.nombre


class TipoTarjeta(models.Model):
	nombre = models.CharField(unique=True, max_length=30)
	descripcion = models.CharField(max_length=300, null=False)
	class Meta:
		verbose_name_plural = 'Tipos de tarjeta'
	def __str__(self):
		return self.nombre


class Tarjeta(models.Model):
	nombre = models.CharField(unique=True, max_length=40)
	tipo_tarjeta = models.ForeignKey(TipoTarjeta, on_delete=models.CASCADE, null= False)
	banco = models.ForeignKey(Banco, on_delete=models.CASCADE, null= True)
	habilitada = models.BooleanField(default=True)

	def __str__(self):
		return self.nombre

class Pago(models.Model):
	monto = models.DecimalField(max_digits=16, decimal_places=2)
	# TODO: PROMOCION
	# Si no tiene tarjeta asociada, el pago fue realizado en efectivo
	tarjeta = models.ForeignKey(Tarjeta, on_delete=models.CASCADE, blank=True, null=True)

	def __str__(self):
		if self.tarjeta:
			return '$%.2f con tarjeta %s' % (self.monto, self.tarjeta)
		return '$%.2f en efectivo' % self.monto

class Entrada(models.Model):
	butaca = models.ForeignKey(Butaca, blank=False, null=False)
	funcion = models.ForeignKey(Funcion, blank=False, null=False)

	def estadoentrada_current(self):
		return self.estadoentrada_set.latest('fecha')

	class Meta:
		ordering = ['butaca__fila', 'butaca__columna']
		unique_together = (('butaca','funcion'),)

	def __str__(self):
		return '%s %s' % (str(self.funcion), str(self.butaca))

	def reservar(self, usuario, codigo):
		ee = EstadoEntrada.objects.create(entrada=self, codigo=codigo, usuario=usuario, estado=EstadoEntrada.R)
		ee.save()
	def cancelar(self):
		ee = EstadoEntrada.objects.create(entrada=self, estado=EstadoEntrada.D)
		ee.save()
	def liberar(self):
		ee = EstadoEntrada.objects.create(entrada=self, estado=EstadoEntrada.L)
		ee.save()
	def vender(self, usuario, codigo, precio, promo, pago):
		ee = EstadoEntrada.objects.create(
			entrada=self,
			estado=EstadoEntrada.P,
			codigo=codigo,
			usuario=usuario,
			precio=precio,
			promo=promo,
			pago=pago
		)
	def reservable(self, *args):
		return self.estadoentrada_current().reservable(*args)
	def cancelable(self, *args):
		return self.estadoentrada_current().cancelable(*args)
	def liberable(self, *args):
		return self.estadoentrada_current().liberable(*args)
	def vendible(self, *args):
		return self.estadoentrada_current().vendible(*args)

class EstadoEntrada(models.Model):
	# Estados Posibles de una Entrada <<enumeration>>
	D, D_TEXT = ("D", "Disponible")
	R, R_TEXT = ("R", "Reservada")
	P, P_TEXT = ("P", "Paga")
	L, L_TEXT = ("L", "Libre")

	ESTADO_CHOICES = (
		(D, D_TEXT),
		(R, R_TEXT),
		(P, P_TEXT),
		(L, L_TEXT),
	)

	def reservable(self):
		return self.entrada.butaca.disponibilidad==Butaca.N and self.estado==EstadoEntrada.D
	def cancelable(self, user):
		return self.estado==EstadoEntrada.R and self.usuario==user
	def liberable(self):
		return self.estado==EstadoEntrada.D or self.estado==EstadoEntrada.R
	def vendible(self, user):
		return self.estado==EstadoEntrada.D or self.estado==EstadoEntrada.L or (self.estado==EstadoEntrada.R and self.usuario==user)

	# id
	estado = models.CharField("Estado", max_length=26, choices=ESTADO_CHOICES, blank=False, null=False)
	entrada = models.ForeignKey(Entrada,on_delete=models.CASCADE, null=False)
	# sha256 -> 64 bytes de digesto hexadecimal
	codigo = models.CharField(null=True, max_length=64)
	fecha = models.DateTimeField("Fecha de cambio de estado", auto_now=True)
	# Si la entrada fue vendida, si o si tendra que tener asociado un pago
	pago = models.ForeignKey(Pago, blank=True, null=True)
	usuario = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
	precio = models.ForeignKey(Precio, on_delete=models.CASCADE, null=True, blank=True)
	promo = models.ForeignKey(Promocion, on_delete=models.CASCADE, null=True, blank=True)


	def is_newest(self):
		return self == EstadoEntrada.objects.filter(entrada=self.entrada).latest('fecha')

	class Meta:
		ordering = ['-fecha']
		unique_together = (('entrada', 'fecha'),)

	def __str__(self):
		return str(self.entrada)

def _funcion_crear_entradas(instance, created, raw, **kwargs):
	if not created or raw:
		return
	for b in instance.sala.butaca_set.all():
		e,created = Entrada.objects.get_or_create(butaca=b,funcion=instance)
		ee,created = EstadoEntrada.objects.get_or_create(entrada=e, estado=EstadoEntrada.D)
	instance.save()

models.signals.post_save.connect(_funcion_crear_entradas, sender=Funcion, dispatch_uid='_funcion_crear_entradas')
