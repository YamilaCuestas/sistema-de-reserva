import json

from django import forms
from django.db.models import Q
from django.utils import timezone
from django.utils.html import format_html
from django.contrib import admin
from django.contrib.admin import DateFieldListFilter
from django.urls import reverse
from django.contrib.admin import DateFieldListFilter
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter

from . import models
from .forms import PrecioForm
from reservas.models import Entrada, EstadoEntrada

class FechaFuncionFilter(admin.RelatedFieldListFilter):
	def has_output(self):
		return True

@admin.register(models.TipoPrecio)
class TipoPrecioAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'nap', 'descripcion')
	ordering = ['nombre']
	search_fields = ('nombre',)


@admin.register(models.Precio)
class PrecioAdmin(admin.ModelAdmin):
	list_display = ('tipo_precio', 'desde', 'hasta', 'monto', 'habilitado')
	def get_readonly_fields(self, request, obj):
		if obj:
			return ('tipo_precio', 'desde', 'hasta', 'monto')
		return tuple()
	def has_delete_permission(self, request, obj=None):
		return False
	form = PrecioForm


@admin.register(models.Funcion)
class FuncionAdmin(admin.ModelAdmin):
	list_display = ('pelicula', 'sala', 'fecha', 'fecha_fin')
	search_fields = ('pelicula__titulo','pelicula__titulo_original')
	ordering = ('pelicula__titulo',)
	list_filter = (
		('sala', RelatedDropdownFilter),
		('fecha', DateFieldListFilter),
	)

	def formfield_for_manytomany(self, db_field, request, **kwargs):
		print(db_field)
		if db_field.name == 'precios':
			kwargs['queryset'] = models.Precio.objects.filter(
				Q(habilitado=True), Q(hasta=None) | Q(hasta__gte=timezone.now())
			)
		return super(FuncionAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

	def view_on_site(self, obj):
		return reverse('reservas-reserve', kwargs={'id_funcion': obj.id})

	change_form_template = 'admin/funciones/change_funcion.html'
	def change_view(self, request, object_id, form_url='', extra_context=None):
		funcion = models.Funcion.objects.get(pk=object_id)
		entradas = funcion.entrada_set.all()
		extra_context = extra_context or {}
		extra_context['entradas'] = json.dumps([
			{
				'id_entrada': ent.id,
				'estado': ent.estadoentrada_set.first().estado,
				'butaca_disponibilidad': ent.butaca.disponibilidad,
				'butaca_fila': ent.butaca.fila,
				'butaca_columna': ent.butaca.columna
			}
			for ent in entradas
		])
		extra_context['funcion'] = funcion
		if (request.method == 'POST'):
			for entrada_id in request.POST.getlist('entradas'):
				entrada = Entrada.objects.get(pk=entrada_id)
				if request.POST.get('entrada-liberar', False) and entrada.liberable():
					entrada.liberar()

		return super().change_view(request, object_id, form_url, extra_context=extra_context)
