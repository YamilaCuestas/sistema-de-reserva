from django.db import models
from django.core.validators import MinValueValidator

class Cuenta(models.Model):
	numero_cuenta = models.IntegerField(validators=(
		MinValueValidator(100000000),
	), unique=True)
	vencimiento = models.DateField(null=False,blank=False)
	titular = models.CharField(max_length=30)
	def saldo(self):
		return sum(a.monto for a in self.acreditacion_set.all()) - sum(p.monto for p in self.pago_set.all())
	def __str__(self):
		return '%s - %s' % (self.numero_cuenta, self.titular)
	class Meta:
		ordering = ['numero_cuenta']

class Acreditacion(models.Model):
	cuenta = models.ForeignKey(Cuenta, on_delete=models.CASCADE)
	monto = models.DecimalField(max_digits=10, decimal_places=2, validators=(
		MinValueValidator(0),
	))
	class Meta:
		verbose_name_plural = 'Acreditaciones'
	def __str__(self):
		return '%s +$%.2f' % (self.cuenta, self.monto)

class Pago(models.Model):
	cuenta = models.ForeignKey(Cuenta, on_delete=models.CASCADE)
	codigo_pago = models.CharField(max_length=64)
	monto = models.DecimalField(max_digits=10, decimal_places=2, validators=(
		MinValueValidator(0),
	))
	def __str__(self):
		return '%s -$%.2f' % (self.cuenta, self.monto)
